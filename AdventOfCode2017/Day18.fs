open System.Collections.Generic;

type RegisterOrInteger = | Register of char
                         | Integer of int64

type Instruction =  | Set of char * RegisterOrInteger
                    | Add of char * RegisterOrInteger
                    | Multiply of char * RegisterOrInteger
                    | Modulo of char * RegisterOrInteger
                    | Sound of char
                    | Recover of char
                    | Jump of RegisterOrInteger * RegisterOrInteger
                    | Send of RegisterOrInteger
                    | Receive of char

let getCharOrInt value =
    let success, result = System.Int64.TryParse (value)
    match success with
    | false -> value |> char |> Register
    | true ->  result |> Integer

let charIntInstruction (ops:string array) =
    let register = char ops.[1]
    let value = getCharOrInt ops.[2]
    (register, value)

let parseInstruction (inst:string) =
    let ops = inst.Split(' ')
    match ops.[0].ToLower() with
    | "set" -> ops |> charIntInstruction |> Set
    | "add" -> ops |> charIntInstruction |> Add
    | "mul" -> ops |> charIntInstruction |> Multiply
    | "mod" -> ops |> charIntInstruction |> Modulo
    | "snd" -> (char ops.[1]) |> Sound
    | "rcv" -> (char ops.[1]) |> Recover
    | "jgz" -> (ops.[1] |> getCharOrInt, ops.[2] |> getCharOrInt) |> Jump
    | "send" -> (ops.[1] |> getCharOrInt ) |> Send
    | "receive" -> (char ops.[1]) |> Receive
    | _ -> failwith "Invalid instruction"

let parseInstructions input =
    input    
        |> Array.filter (System.String.IsNullOrEmpty >> not) 
        |> Array.map (parseInstruction)

let getRegisterName instruction =
    match instruction with
    | Set(x,_)          -> x |> Some
    | Add(x,_)          -> x |> Some
    | Multiply(x,_)     -> x |> Some
    | Modulo(x,_)       -> x |> Some
    | Send(Register x)  -> x |> Some
    | Receive(x)        -> x |> Some
    | _                 -> None

let getRegisters (instructions:Instruction array) =
    instructions 
        |> Array.map (getRegisterName) 
        |> Array.fold (fun acc x -> match x with | None -> acc | Some(x) -> acc @ [x] ) []
        |> List.distinct 
        |> List.map (fun x -> (x, int64 0))
        |> Array.ofList

let getValueFromRegName registers regName =
    registers 
        |> Array.find (fun x -> fst x = regName) 
        |> snd

let getValueFromRegOrInt registers regOrInt =
    match regOrInt with
    | Register x -> x |> getValueFromRegName registers |> int64
    | Integer x -> int64 x 

let setValueOfRegister registers regName value =
    let index = registers |> Array.findIndex (fun x -> fst x = regName)
    registers.[index] <- (regName, value)
    registers

let performBasicOperation registers regName regNameOrValue operation =
    let value = getValueFromRegOrInt registers regNameOrValue
    let newValue = operation (getValueFromRegName registers regName) (value)
    setValueOfRegister registers regName newValue

let processOperation (registers, frequencyPlaying, _, timesSent) (sendQueue: Queue<int64>, receiveQueue : Queue<int64>) instruction = 
    match instruction with
    | Sound reg ->  let frequency = getValueFromRegName registers reg
                    (registers, frequency, int64 1, timesSent);
    | Set (x,y) -> let value = getValueFromRegOrInt registers y
                   let regs = setValueOfRegister registers x value
                   (regs, frequencyPlaying, int64 1, timesSent)
    | Add (x,y) -> let regs = performBasicOperation registers x y (+)
                   (regs, frequencyPlaying, int64 1, timesSent)
    | Multiply (x,y) -> let regs = performBasicOperation registers x y (*)
                        (regs, frequencyPlaying, int64 1, timesSent)
    | Modulo (x,y) -> let regs = performBasicOperation registers x y (%)
                      (regs, frequencyPlaying, int64 1, timesSent)
    | Recover (x) -> let value = getValueFromRegName registers x
                     if value <> int64 0 then
                         (registers, frequencyPlaying, int64 9999, timesSent)
                     else
                         (registers, frequencyPlaying, int64 1, timesSent)
    | Jump (x,y) -> let value = getValueFromRegOrInt registers x
                    if value > int64 0 then
                        let offset = getValueFromRegOrInt registers y
                        (registers, frequencyPlaying, offset, timesSent)
                    else
                        (registers, frequencyPlaying, int64 1, timesSent)
    | Send (x) -> let value = getValueFromRegOrInt registers x
                  sendQueue.Enqueue(value)
                  (registers, frequencyPlaying, int64 1, timesSent + 1)
    | Receive (x) -> let rec tryReceive tries = 
                        if receiveQueue.Count > 0 then
                            let value = receiveQueue.Dequeue()
                            let regs = setValueOfRegister registers x value
                            (regs, frequencyPlaying, int64 1, timesSent)
                        else
                            if tries > 10 then
                                (registers, frequencyPlaying, int64 9999, timesSent)
                            else
                                System.Threading.Thread.Sleep(100)
                                tryReceive (tries + 1)
                     tryReceive 0


let processOperationsOnRegisters registers instruction (sendqueue, receiveQueue)  =
    let rec loop (registers, frequencyPlaying, offset, timesSent) pos =
        let newPos = int (int64 pos + offset);
        if newPos < 0 || newPos >= (instruction |> Array.length) then
            (registers, frequencyPlaying, offset, timesSent)
        else
            let operation = instruction.[newPos]
            let result = processOperation 
                            (registers, frequencyPlaying, offset, timesSent) 
                            (sendqueue, receiveQueue) 
                            operation
            loop result newPos
    
    let (registers, frequencyPlaying, _, timesSent) = loop (registers, 0L, 0L, 0) 0
    (registers, frequencyPlaying, timesSent)


let part1 input = 
    let instructions = parseInstructions input
    let registers = getRegisters instructions
    let emptyQueues = (Queue<int64>(), Queue<int64>())
    processOperationsOnRegisters registers instructions emptyQueues

//part2
let changeSndAndRcvToReceive (input:string[]) =
    input |> Array.map (fun x -> x.Replace("snd","send").Replace("rcv", "receive"))
let modifyInstructions = changeSndAndRcvToReceive >> parseInstructions 
 
let applyProgramIdToRegisters id registers=
    let index = registers |> Array.tryFindIndex (fun x -> fst x = 'p')
    match index with
        | Some(x) -> registers.[x] <- ('p', id)
        | None -> ()
    registers
let part2 input =
    let modifiedInstructions = modifyInstructions input
    let registers0 = getRegisters modifiedInstructions |> applyProgramIdToRegisters 0L
    let registers1 = getRegisters modifiedInstructions |> applyProgramIdToRegisters 1L
    let queues = (Queue<int64>(), Queue<int64>())
    
    let process0 = async {
        let (_,_, timesSent) = processOperationsOnRegisters 
                                    registers0 
                                    modifiedInstructions 
                                    queues 
        return timesSent
    }
    let process1 = async {
        let (_,_, timesSent) = processOperationsOnRegisters 
                                    registers1 
                                    modifiedInstructions 
                                    (snd queues, fst queues)
        return timesSent
    }
    let result0 = Async.StartAsTask process0
    let result1 = Async.StartAsTask process1
    (result0.Result, result1.Result)

let input = System.IO.File.ReadAllLines (System.IO.Directory.GetCurrentDirectory() + @"\Day18Input.txt")
input |> part1 |> fun (regs, sound, _) -> sound
input |> part2
