let getMiddlePos length =
    match length % 2 with
    | 1 -> (length / 2) + 1
    | _ -> failwith "Cant determine middle pos"

type Grid<'a> = Grid of 'a list list
type NodeStatus = | Infected | Clean | Flagged | Weakened
type Facing = | Up | Left | Right | Down
with 
    static member ToLeft facing=
        match facing with
        | Up -> Left
        | Left -> Down
        | Right -> Up
        | Down -> Right
    static member ToRight facing=
        match facing with
        | Up -> Right
        | Left -> Up
        | Right -> Down
        | Down -> Left
    static member ReverseDirection facing =
        match facing with
        | Up -> Down
        | Left -> Right
        | Right -> Left
        | Down -> Up

type VirusStatus = {
    grid : Grid<char>
    currentPosition : (int * int)
    facing : Facing
    infectionCount: int
}
with 
    static member CreateVirusStatus (Grid grid) =
        let middleX = grid.[0] |> List.length |> getMiddlePos
        let middleY = grid |> List.length |> getMiddlePos
        { 
            grid = grid |> Grid; 
            currentPosition = (middleY-1, middleX-1); 
            facing = Up 
            infectionCount = 0
        }

let parseRow (rowStr:string) =
    rowStr.Replace("\r", "").ToCharArray() |> List.ofSeq

let parseInput (input:string) =
    input.Split('\n') 
        |> Array.map(parseRow) 
        |> List.ofSeq 
        |> Grid

let extendGridIfNecessary (virusStatus:VirusStatus) (y,x) =
    let (Grid grid) = virusStatus.grid
    let (posY, posX) = virusStatus.currentPosition
    let width = grid.[0] |> List.length
    if y < 0  then
        let newLine = List.replicate width '.'
        { virusStatus with grid = (newLine :: grid) |> Grid 
                           currentPosition = (posY + 1, posX)} |> Some
    else if y >= (grid |> List.length) then
        let newLine = List.replicate width '.'
        { virusStatus with grid = (grid @ [newLine]) |> Grid } |> Some
    else if x < 0 then
        { virusStatus with grid = grid |> List.map (fun x -> '.' :: x) |> Grid 
                           currentPosition = (posY, posX + 1)} |> Some
    else if x >= (grid.[y] |> List.length) then
        { virusStatus with grid = grid |> List.map (fun x -> x @ ['.']) |> Grid } |> Some
    else
        None

let getUnderlyingField virusStatus =
    let (Grid grid) = virusStatus.grid
    let (y, x) = virusStatus.currentPosition
    match grid.[y].[x] with
    | '.' -> Clean
    | '#' -> Infected
    | 'W' -> Weakened
    | 'F' -> Flagged
    | _ -> failwith (sprintf "Invalid character at pos %d %d" y x)

let moveHead virusStatus =
    let fn = match getUnderlyingField virusStatus with
             | Clean -> Facing.ToLeft
             | Weakened -> id
             | Infected -> Facing.ToRight
             | Flagged -> Facing.ReverseDirection 
    { virusStatus with facing = fn virusStatus.facing}

let moveForward virusStatus =
    let getNextPosition (y,x)= 
        match virusStatus.facing with
        | Up -> (y-1,x)
        | Down -> (y+1,x)
        | Left -> (y,x-1)
        | Right -> (y,x+1)
    
    let nextPosition = getNextPosition virusStatus.currentPosition
    match extendGridIfNecessary virusStatus nextPosition with
    | None -> { virusStatus with currentPosition = nextPosition }    
    | Some(x) -> { x with currentPosition = getNextPosition x.currentPosition }
    
let getNextFieldStatusPart1 virusStatus =
    match virusStatus |> getUnderlyingField  with
                              | Clean -> Infected
                              | Infected -> Clean

let getNextFieldStatusPart2 virusStatus =
    match virusStatus |> getUnderlyingField  with
                              | Clean -> Weakened
                              | Weakened -> Infected
                              | Infected -> Flagged
                              | Flagged -> Clean

let changeUnderLiningField getFieldStatus virusStatus =
    let nextFieldStatus = virusStatus |>  getFieldStatus 
    let changeTo = match nextFieldStatus  with
                   | Clean -> '.'
                   | Infected -> '#'
                   | Weakened -> 'W'
                   | Flagged -> 'F'
    let (Grid grid) = virusStatus.grid
    let (yPos, xPos) = virusStatus.currentPosition
    let newGrid = grid |> List.mapi (fun i x -> if i = yPos then x |> List.mapi (fun i x -> if i = xPos then changeTo else x) else x)
    match nextFieldStatus with
    | Infected -> { virusStatus with grid = newGrid |> Grid; infectionCount = virusStatus.infectionCount + 1 }
    | _ -> { virusStatus with grid = newGrid |> Grid }
    

let burstPart1 = moveHead >> changeUnderLiningField getNextFieldStatusPart1 >> moveForward
let burstPart2 = moveHead >> changeUnderLiningField getNextFieldStatusPart2 >> moveForward


let burstNTimes burstPart n virusStatus =
    let rec burstNTimes n virusStatus =
        match n with
        | 0 -> virusStatus
        | _ -> virusStatus |> burstPart |> burstNTimes (n-1)
    burstNTimes n virusStatus


let path = System.IO.Directory.GetCurrentDirectory() + @"\AdventOfCode2017\Day22Input.txt"
let input = System.IO.File.ReadAllText path

let part1 input =
    let virusStatus = input |> parseInput |> VirusStatus.CreateVirusStatus
    virusStatus |> burstNTimes burstPart1 10000 |> (fun x -> x.infectionCount)

let part2 input =
    let virusStatus = input |> parseInput |> VirusStatus.CreateVirusStatus
    virusStatus |> burstNTimes burstPart2 10000000 |> (fun x -> x.infectionCount)

part1 input
part2 input
