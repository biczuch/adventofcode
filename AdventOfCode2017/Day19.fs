open System.IO;

type Direction = | ToLeft |ToRight | ToUp | ToDown

let findStartPos (input:string array) =
    (0, input.[0] |> (fun x -> x.IndexOf("|")))

let getCharacterFromPos (input: string array) (x,y) =
    input.[x].[y]

let moveToNextPosBasedOnDirection (x,y) direction =
    match direction with
    | ToLeft -> (x , y - 1)
    | ToRight -> (x, y + 1)
    | ToUp -> (x - 1, y)
    | ToDown -> (x + 1, y)

let findNextPosOnPlus (input:string array) (x,y) direction =
    let perpendicularPositions = [(input.[x].[y+1], (x,y+1), ToRight);
                                  (input.[x].[y-1], (x,y-1), ToLeft);
                                  (input.[x-1].[y], (x-1,y), ToUp);
                                  (input.[x+1].[y], (x+1,y), ToDown);]
    
    let directionToRemove = match direction with
                            | ToUp -> ToDown
                            | ToDown -> ToUp
                            | ToLeft -> ToRight
                            | ToRight -> ToLeft

    perpendicularPositions 
        |> List.filter (fun (character,_,_) -> character <> ' ')
        |> List.filter (fun (_, _, direction) -> direction <> directionToRemove )
        |> List.exactlyOne
        |> fun (_,newPos, direction) -> (newPos, direction)

let rec moveToNextPos input pos direction characters steps =
    let currentPosCharacter = getCharacterFromPos input pos
    printf "pos x = %d y = %d char=%c\n" (fst pos) (snd pos) currentPosCharacter
    
    let nextPos = moveToNextPosBasedOnDirection pos direction
    match currentPosCharacter with
    | x when x = '|' || x = '-' -> moveToNextPos input nextPos direction characters (steps + 1)
    | x when int x >= 65 && int x <= 90 -> moveToNextPos input nextPos direction (characters @ [x]) (steps + 1)
    | '+' -> let (newPos, newDirection) = findNextPosOnPlus input pos direction
             moveToNextPos input newPos newDirection characters (steps + 1)
    | ' ' -> (characters, steps)
    | _ -> failwith "Something went wrong!"

let part1 input = let startPos = input |> findStartPos 
                  moveToNextPos input startPos ToDown [] 0

let path = Directory.GetCurrentDirectory() + @"\Day19Input.txt"
let input = File.ReadAllLines(path)

input |> part1
             