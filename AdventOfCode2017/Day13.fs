open System.IO;

let createLayerTuple (layerNo, depth) = (layerNo, 2*(depth-1) ,depth)
let parseRow (inputRow:string) =
    inputRow.Replace(" ", "").Split(':')
      |> Array.map int
      |> Array.pairwise
      |> Array.exactlyOne
       
let parseInput (input:string) =
    input.Split('\n')
        |> Array.map(parseRow >> createLayerTuple)

let isCoughtInLayer delay (layerNo, repeat, depth) =
    (delay + layerNo) % repeat = 0

let getCaughtLayers layers =
    layers |> Array.filter (isCoughtInLayer 0)

let calculateSeverity =
    getCaughtLayers >> Array.sumBy (fun (x,y,z) -> x * z)

let findFirst0CaughtDelay layers =
    let rec loop delay layers =
        match layers |> Array.forall (isCoughtInLayer delay >> not) with
        | true -> delay
        | false -> loop (delay + 1) layers

    loop 0 layers

let input = File.ReadAllText(Directory.GetCurrentDirectory() + @"\Day13Input.txt");
let layers = input |> parseInput
let part1 = layers |> calculateSeverity
let part2 = layers |> findFirst0CaughtDelay