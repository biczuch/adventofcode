
type Port = | Port of (int * int)
with
    static member CreatePort (x,y) =
        (x,y) |> Port

type PortNode = {
    port : Port
    portNumber: int
    nextPorts: PortNode list option
}
with 
    static member CreatePortNode port portNo =
        {
            port = port
            portNumber = portNo
            nextPorts = None
        }

let parseRow (row:string) =
    row.Split('/') 
        |> Array.map (int) 
        |> Array.pairwise
        |> Array.map Port.CreatePort
        |> Array.exactlyOne

let parseInput (input:string) =
    input.Split('\n') 
        |> Array.map parseRow
        |> Array.toList

let portMatches portVal (Port(x,y))  =
    x = portVal || y = portVal
let isStatringPort = portMatches 0

let getStartingPorts ports =
    ports |> List.filter isStatringPort

let getNextValue (Port(x,y)) value =
    if x = value then y
    else if y = value then x
    else failwith "Something wrong"

let createNodeFromPort port value =
    let nextValue = getNextValue port value
    PortNode.CreatePortNode port nextValue

let createPortFrompredecessor port predecessor =
    createNodeFromPort port predecessor.portNumber

let makeTrees ports = 
    let startingPorts = ports 
                        |> getStartingPorts 
                        |> List.map (fun x -> createNodeFromPort x 0) 
                                            
    let rec createTree portNode remainingPorts =
        let newRemaining = remainingPorts |> List.filter (fun x -> x <> portNode.port)
        match newRemaining with
        | [] -> None
        | _ -> let nextPorts = newRemaining
                                |> List.filter (portMatches portNode.portNumber)
               match nextPorts with
               | [] -> portNode |> Some
               | _ -> let nextPortNodes = nextPorts |> List.map (fun x -> createPortFrompredecessor x portNode)
                                          |> List.map (fun x -> createTree x newRemaining )
                                          |> List.filter (fun x -> x.IsSome)
                                          |> List.map (fun x -> x.Value)
                                          |> (fun x -> match x with
                                                       | [] -> None
                                                       | x -> Some(x))
                      { portNode with nextPorts = nextPortNodes } |> Some
    
    startingPorts 
    |> List.map (fun x -> createTree x ports) 
    |> List.filter (fun x -> x.IsSome) 
    |> List.map (fun x -> x.Value)

let rec getListFromTree (portNode:PortNode) (output:Port list) (Alloutputs:Port list list) : Port list list=
    let newOutput = output @ [portNode.port]
    let newAllOutput = Alloutputs @ [newOutput]
    match portNode.nextPorts with
    | None -> newAllOutput
    | Some(x) -> x |> List.map (fun x -> getListFromTree x newOutput newAllOutput) |> List.collect id

let rec getValueOfTree tree =
    let (Port(x,y)) = tree.port
    let value = x + y;
    let subNodesValue = match tree.nextPorts with
                        | None -> 0
                        | Some(x) -> x |> List.map getValueOfTree |> List.max
    value + subNodesValue

let getBridges ports = 
    ports |> makeTrees |> List.map (fun x -> getListFromTree x [] []) |> List.collect id |> List.distinct

let getLongestBridges ports= 
    ports |> getBridges 
    |> List.groupBy (fun x -> x |> List.length) 
    |> List.maxBy (fun x -> x |> fst) 
    |> fun x -> x |> snd

let part1 input =  
    parseInput input |> makeTrees |> List.map getValueOfTree |> List.max

let part2 input =
    parseInput input 
        |> getLongestBridges 
        |> List.map (fun x -> x |> List.sumBy (fun (Port(x,y)) -> x + y))
        |> List.max

let path = System.IO.Directory.GetCurrentDirectory() + @"\AdventOfCode2017\Day24Input.txt"
let input = System.IO.File.ReadAllText path

part1 input
part2 input