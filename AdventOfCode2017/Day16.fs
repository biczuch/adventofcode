open System.IO

type Operation = | Spin of int
                 | Exchange of int * int
                 | Partner of char * char

let spin (programs:string) position =
    let index = (programs.Length + 1 - position)
    let a = programs.Substring (0, (index - 1))
    let b = programs.Substring (index - 1)
    b + a

let exchange (programs:string) posA posB =
    let chars = programs.ToCharArray() 
    let temp = chars.[posA]
    chars.[posA] <- chars.[posB]
    chars.[posB] <- temp
    chars |> Array.fold (fun acc x -> acc + x.ToString()) ""

let partner (programs:string) nameA nameB =
    let posA = programs.IndexOf (nameA.ToString())
    let posB = programs.IndexOf (nameB.ToString())
    exchange programs posA posB

let getLettersOrPrograms (input:string) =
    let positions = input.Substring(1).Split('/')
    (positions.[0], positions.[1])

let getOperation (input:string) =
    let firstChar = input.[0].ToString().ToLowerInvariant();
    match firstChar with
    | "s" -> input.Substring(1) |> int |> (fun x -> Spin(x))
    | "x" -> input |> getLettersOrPrograms |> (fun x -> Exchange(x |> fst |> int, x |> snd |> int))
    | "p" -> input |> getLettersOrPrograms |> (fun x -> Partner(x |> fst |> char, x |> snd |> char))
    | _ -> failwith "Couldn't parse operation from input string"

let parseInput (input:string) =
    input.Split(',') |> Array.map getOperation |> List.ofArray

let processOperation str operation =
    match operation with
    | Spin(x) -> spin str x
    | Exchange(x,y) -> exchange str x y
    | Partner(x,y) -> partner str x y

let rec performDance str operations =
    match operations with
    | [] -> str
    | x::xs -> performDance (processOperation str x) xs

let rec findCycle str operations times (repetitions: string list) = 
    match times with
        | 0 -> repetitions.Length
        | n -> let x = performDance str operations
               if repetitions |> List.contains x then
                    repetitions.Length
               else
                    findCycle x operations (n-1) (repetitions @ [x])
                    
let rec performDanceNTimes str operations times =
    match times with
    | 0 -> str
    | n -> printf "step = %d str = %s\n" times str
           performDanceNTimes (performDance str operations) operations (n - 1) 
    
let solvePart2 programs operations times =
    let cycle = findCycle programs operations times []
    performDanceNTimes programs operations (times % cycle)

let input = File.ReadAllText (System.IO.Directory.GetCurrentDirectory() + @"\AdventOfCode2017\Day16Input.txt")
let programs = [97..112] |> List.map (char) |> List.fold (fun acc x -> acc + x.ToString() ) ""
let operations = input |> parseInput

solvePart2 programs operations 1000000000