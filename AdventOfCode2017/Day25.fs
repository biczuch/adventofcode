type State = |A|B|C|D|E|F
open System.Web.ModelBinding
type Tape = Tape of Map<int, bool>

type MoveType = |Left|Right

type StateConditions = {
    write: bool
    move: MoveType
    nextState: State
}

type StateDetails =
    {
        state: State
        if0: StateConditions
        if1: StateConditions
    }
    with
        static member GetStateCondition stateDetails value =
            match value with
            | true -> stateDetails.if1
            | false -> stateDetails.if0

type TuringMachine = {
    tape : Tape
    currentPosition: int
    currentState: State
}
with 
    static member CreateTuringMachine beginState =
        { 
            tape = Map.empty.Add (0,false) |> Tape
            currentPosition = 0
            currentState = beginState
        }
    static member GetCurrentValue turingMachine =
        let (Tape tape) = turingMachine.tape
        tape.TryFind turingMachine.currentPosition
    static member CalculateChecksum turingMachine =
        let (Tape tape) = turingMachine.tape
        tape 
        |> Map.filter (fun _ v-> v ) 
        |> Map.toList 
        |> List.length
    static member InsertValueToTape value turingMachine  =
        let (Tape tape) = turingMachine.tape
        if tape.ContainsKey value then
            turingMachine
        else
            let newTape = tape.Add (value, false) |> Tape
            { turingMachine with tape = newTape}
let goToNextState turingMachine =
    let nextState = match turingMachine.currentState with
                    | A -> B
                    | B -> A
                    | _ -> failwith "Invalid state"
    { turingMachine with currentState = nextState }

let changeTape value turingMachine = 
    let (Tape tape) = turingMachine.tape
    let newTape = tape 
                    |> Map.remove turingMachine.currentPosition 
                    |> Map.add turingMachine.currentPosition value 
                    |> Tape
    { turingMachine with tape = newTape}

let moveSlot moveType turingMachine =
    let newPosition = match moveType with
                      | Left -> turingMachine.currentPosition - 1
                      | Right -> turingMachine.currentPosition + 1
    turingMachine 
        |> TuringMachine.InsertValueToTape newPosition
        |> fun x -> { x with currentPosition = newPosition }

let changeState nextState turingMachine =
    { turingMachine with currentState = nextState }

let doCondition condition turingMachine =
    turingMachine 
    |> changeTape condition.write
    |> moveSlot condition.move
    |> changeState condition.nextState

let preformState turingMachine states =
    let state = states |> Set.toList |> List.find (fun x -> x.state = turingMachine.currentState)
    match turingMachine |> TuringMachine.GetCurrentValue with
    | None -> failwith "Something wrong"
    | Some(true) -> turingMachine |> doCondition state.if1
    | Some(false) -> turingMachine |> doCondition state.if0

let rec preformIterations turingMachine states iteration =
    match iteration with
    | 0 -> turingMachine
    | _ -> let newTM = preformState turingMachine states
           preformIterations newTM states (iteration - 1)
           

let part1 states beginState iterations =
    let tm = TuringMachine.CreateTuringMachine beginState 
    preformIterations tm states iterations |> TuringMachine.CalculateChecksum

let stateA = {
    state = A
    if0 = {
            write = true
            move = Right
            nextState = B
          }
    if1 = {
            write = false
            move = Left
            nextState = E
          }
}

let stateB = {
    state = B
    if0 = {
            write = true
            move = Left
            nextState = C
    }
    if1 = {
            write = false
            move = Right
            nextState = A
    }
}

let stateC = {
    state = C
    if0 = {
            write = true
            move = Left
            nextState = D
    }
    if1 = {
            write = false
            move = Right
            nextState = C
    }
}

let stateD = {
    state = D
    if0 = {
            write = true
            move = Left
            nextState = E
    }
    if1 = {
            write = false
            move = Left
            nextState = F
    }
}

let stateE = {
    state = E
    if0 = {
            write = true
            move = Left
            nextState = A
    }
    if1 = {
            write = true
            move = Left
            nextState = C
    }
}

let stateF = {
    state = F
    if0 = {
            write = true
            move = Left
            nextState = E
    }
    if1 = {
            write = true
            move = Right
            nextState = A
    }
}


let states = [stateA;stateB;stateC;stateD;stateE;stateF] |> Set.ofList

part1 states A 12208951