type Buffer = {
    currentPosition : int
    state : int list
    byStep: int
    step : int
    endStep: int
}
with
    static member CreateBuffer byStep endStep=
        { currentPosition = 0; state = [0]; byStep = byStep; step = 1; endStep = endStep }

let moveBySteps buffer = 
    if buffer.state |> List.isEmpty then
        { buffer with currentPosition = 0 }
    else
        let newPosition = (buffer.currentPosition + buffer.byStep) % (buffer.state |> List.length)
        { buffer with currentPosition = newPosition }

let insertValueAfterPosition buffer position element =
    let rec loop buffer pos = 
        match buffer with
        | [] -> [element]
        | x::xs -> if pos = position then
                        (x::element::xs)
                   else
                      x::(loop xs (pos + 1))
    loop buffer 0

let incrementBufferCurrentPosition buffer =
    { buffer with currentPosition = buffer.currentPosition + 1; step = buffer.step + 1}

let insertNewValueToBuffer buffer = 
    let newBufferState = insertValueAfterPosition buffer.state buffer.currentPosition buffer.step
    { buffer with state = newBufferState}

let findNextAfterEndStep buffer =
    let index = buffer.state |> List.findIndex (fun x -> x = buffer.endStep) |> (+) 1
    buffer.state.[index]

let rec solvePart1 buffer =    
    match buffer.step = buffer.endStep + 1 with
    | true  -> buffer
    | false -> buffer 
                |> moveBySteps
                |> insertNewValueToBuffer 
                |> incrementBufferCurrentPosition
                |> solvePart1


type Buffer2 = {
    currentPosition : int
    byStep: int
    lastInsertedAfterPos0 : int
    iteration: int
    stopIteration: int
}

let rec solvePart2 (buffer:Buffer2) =
    if buffer.iteration >= buffer.stopIteration then
        buffer
    else
        let nextPosition = match buffer.iteration with
                           | x when x < 0 -> failwith "invalid iteration"
                           | x when x = 0 -> 0
                           | _ -> (buffer.currentPosition + buffer.byStep) % buffer.iteration
        let lastInsertedInPos0 = match nextPosition = 0 with
                                 | true  -> buffer.iteration
                                 | false -> buffer.lastInsertedAfterPos0
        let newBuffer = { buffer with 
                                      lastInsertedAfterPos0 = lastInsertedInPos0 
                                      iteration = buffer.iteration + 1
                                      currentPosition = nextPosition + 1 }
        solvePart2 newBuffer

let buffer = Buffer.CreateBuffer 382 2017
buffer |> solvePart1 |> findNextAfterEndStep

let buffer2 = {
    currentPosition = 0
    byStep = 382
    lastInsertedAfterPos0 = 0
    iteration = 0
    stopIteration = 50000000
}
buffer2 |> solvePart2 |> fun x -> x.lastInsertedAfterPos0
