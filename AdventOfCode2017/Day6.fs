type Bank = Bank of int
    with 
        static member CreateBank x = 
            x |> Bank

        static member Deconstruct x =
            let (Bank a) = x
            a

type Banks = Banks of Bank array
    with 
        static member CreateBanks banksArray =
            banksArray |> Array.map Bank.CreateBank |> Banks
        
        static member Deconstruct banks =
            let (Banks a) = banks
            a
   
let chooseBiggestNumberIndex banks =
    banks 
        |> Banks.Deconstruct
        |> Array.indexed 
        |> Array.maxBy snd
        |> fst

let doOnBanksIndex f index banks =
    banks 
        |> Banks.Deconstruct 
        |> Array.mapi (fun i x -> if i = index then (f x) else x)

let emptyBankAtIndex banks index = 
    banks 
        |> doOnBanksIndex (fun _ -> Bank.CreateBank(0)) index 

let emptyBiggestBank banks = 
    banks 
        |> chooseBiggestNumberIndex 
        |> emptyBankAtIndex banks
        |> Banks

let incrementBankValue bank =
    bank |> Bank.Deconstruct |> (+) 1 |> Bank.CreateBank

let incrementAtBankIndex index banks =
    banks 
        |> doOnBanksIndex incrementBankValue index 
        |> Banks

let getBiggestValueFromBanks banks =
    banks 
        |> Banks.Deconstruct 
        |> Array.maxBy (fun x -> x |> Bank.Deconstruct) 
        |> Bank.Deconstruct

let getNextIndex banks index =
    let banksLength = banks |> Banks.Deconstruct |> Array.length
    match index with
        | x when x < 0 -> failwith "Index cannot be negative"
        | x when x < (banksLength - 1) -> x + 1
        | _ -> 0

let rec distributeValue index value banks =
    match value with
        | 0 -> banks
        | x -> let incrementedBanks = banks |> incrementAtBankIndex index
               let nextIndex = index |> getNextIndex incrementedBanks
               distributeValue  nextIndex (x - 1) incrementedBanks

let getCycles bankHistory =
    List.length bankHistory - 1

let bankAlreadyRepeated banks banksHistory =
    banksHistory |> List.exists (fun x -> x = banks)

let distributeBiggestValue banks =
    let biggestValue = banks |> getBiggestValueFromBanks
    let nextIndex = banks |> chooseBiggestNumberIndex |> getNextIndex banks
    banks
        |> emptyBiggestBank
        |> distributeValue nextIndex biggestValue

let convertToString banks =
    banks 
        |> Banks.Deconstruct 
        |> Array.map (Bank.Deconstruct >> string)
        |> Array.reduce (fun acc x -> acc + " " + x )
        
let printBankHistory bankHistory =
    printfn "----------------------------"
    bankHistory
        |> List.map convertToString
        |> List.iter (printfn "%s\n")
    printfn "----------------------------"
       
let distributeValueUntilFirstRepeat banks =
    let rec repeater banksHistory banks  =
        if bankAlreadyRepeated banks banksHistory then
            banksHistory @ [banks]
        else
            let newBankHistory = banksHistory @ [banks]
            banks 
                |> distributeBiggestValue
                |> repeater newBankHistory
    
    banks |> repeater []

let parseInput (input : string) =
    input.Split(' ') |> Array.map int
let createBanksFromInput = parseInput >> Banks.CreateBanks

let getLastBank banks =
    banks |> List.last

let findIndexOfFirstOccurenceOfLastBank banks =
    let toFind = banks |> getLastBank 
    banks |> List.findIndex (fun x -> x = toFind)

let getOffset banks = 
    ((banks |> List.length) - 1) - (banks |> findIndexOfFirstOccurenceOfLastBank)

let banksHistory = "0 5 10 0 11 14 13 4 11 8 8 7 1 4 12 11" |> createBanksFromInput  |> distributeValueUntilFirstRepeat
let answerPart1 = banksHistory |> getCycles
let answerPart2 = banksHistory |> getOffset
