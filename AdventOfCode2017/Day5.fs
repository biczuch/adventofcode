type Maze = {
    currentStep : int
    currentPosition: int
    instructions: int array
}
with
    static member private Default =
        {
            currentStep = 1
            currentPosition = 0
            instructions = [||]
        }
    static member Create instructions =
        { Maze.Default with instructions = instructions }

let isOutsideMaze maze =
    let isOutOfBoundsLeft = maze.currentPosition < 0
    let isOutOfBoundsRight = maze.currentPosition >= (maze.instructions |> Array.length )
    isOutOfBoundsLeft || isOutOfBoundsRight

let modifyMazeInstructions newValueCalculations maze  = 
    let mapper i x = 
        if i = maze.currentPosition then 
            newValueCalculations x 
        else 
            x

    let newInstructions = maze.instructions |> Array.mapi mapper
    { maze with instructions = newInstructions }

let incrementStep maze =
    { maze with currentStep = maze.currentStep + 1 }     

let part1ValueCalculations x =
    x + 1
let part2ValueCalculations x =
    if x >= 3 then
        x - 1
    else
        x + 1
    
let changeCurrentPosition newPosition maze =
   { maze with currentPosition = newPosition }

let determineNextPosition maze =
    let offset = maze.instructions |> Array.item maze.currentPosition
    maze.currentPosition + offset

let jump maze =
    let nextPosition = determineNextPosition maze
    maze
        |> modifyMazeInstructions part2ValueCalculations
        |> incrementStep
        |> changeCurrentPosition nextPosition

let rec jumpUntilOutsideMaze maze  =
    match isOutsideMaze maze with
        | true -> maze
        | false -> jump maze |> jumpUntilOutsideMaze

let getMazeStep maze = 
    maze.currentStep - 1

let parseInput (input:string) =
    input.Split('\n') |> Array.filter (System.String.IsNullOrEmpty >> not) |> Array.map int

let parseAndCreateMaze = parseInput >> Maze.Create

let getStepFromInput1 = parseAndCreateMaze 
                           >> jumpUntilOutsideMaze
                           >> getMazeStep

let input = "0
3
0
1
-3"

getStepFromInput1 input