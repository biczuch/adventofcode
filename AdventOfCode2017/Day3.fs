type Point = { 
    Coordinates : int * int
    PointNumber : int
    PointValue : int
    PreviousPoint : Point option
}

type Direction = | Right
                 | Left
                 | Up
                 | Down

let createPoint x y pointNumber pointValue previousPoint =
    {
        Coordinates = (x,y)
        PointNumber = pointNumber
        PointValue = pointValue
        PreviousPoint = previousPoint
    }

let getNextDirection = function
    | Right -> Up
    | Left -> Down
    | Down -> Right
    | Up -> Left

let getCoordinates direction (x,y) =
    match direction with
        | Right ->  ( x + 1, y)
        | Left ->   ( x - 1, y)
        | Up ->     ( x, y + 1)
        | Down ->   ( x, y - 1)

let getNeigbour (x,y) allPoints =
    let neighbour = List.tryFind (fun p -> p.Coordinates = (x,y)) allPoints
    match neighbour with
        | Some(n) -> n.PointNumber
        | None -> 0


let getSumOfNeighbours (x,y) allPoints =
    let ULNeighbour = getNeigbour (x - 1, y + 1) allPoints
    let UNeighbour  = getNeigbour (x, y + 1) allPoints
    let URNeighbour = getNeigbour (x + 1, y + 1) allPoints
    let LNeighbour  = getNeigbour (x - 1, y) allPoints
    let RNeighbour  = getNeigbour (x + 1, y) allPoints
    let DLNeighbour = getNeigbour (x - 1, y - 1) allPoints
    let DNeighbour  = getNeigbour (x, y - 1) allPoints
    let DRNeighbour = getNeigbour (x + 1, y - 1) allPoints

    ULNeighbour + UNeighbour + URNeighbour + LNeighbour + RNeighbour + DLNeighbour + DNeighbour + DRNeighbour
    
let getPreviousPoints point =
    let rec previousPointsListCreator point list =
        match point.PreviousPoint with
            | Some(x) -> list @ [point] @ (previousPointsListCreator x list)
            | None -> list @ [point]
    
    previousPointsListCreator point []

let createNextPoint previousPoint direction =
    let newCoordinates = getCoordinates direction previousPoint.Coordinates
    // let newValue = previousPoint.PointNumber + 1
    let newValue = getSumOfNeighbours (fst newCoordinates, snd newCoordinates) (getPreviousPoints previousPoint)
    createPoint (fst newCoordinates) (snd newCoordinates) newValue (Some(previousPoint))

let rec createPointRow times direction previousPoint =
    match times with
        | 0 -> previousPoint
        | n when n > 0 ->  let createdPoint = createNextPoint previousPoint direction
                           createPointRow (n-1) direction createdPoint
        | _ -> failwith "Times parameter must be positive number"

let rec goToPointNumber pointNumber fromPoint =
    if fromPoint.PointNumber = pointNumber then
        fromPoint
    else
        match fromPoint.PreviousPoint with
            | Some(pPoint) -> goToPointNumber pointNumber pPoint
            | None -> failwith "Couldn't find point"

let getPointCount step =
    int (System.Math.Ceiling ((float step) / 2.0))

let rec createPlaneStep step direction startingPoint endPointNumber =
   let repetition = getPointCount step
   let lastPointInRow = createPointRow repetition direction startingPoint

   match lastPointInRow.PointNumber with
       | n when n < endPointNumber -> createPlaneStep (step + 1) (getNextDirection direction) lastPointInRow endPointNumber
       | _ -> goToPointNumber endPointNumber lastPointInRow

let createPlane startingPoint endPointNumber = 
    createPlaneStep 1 Direction.Right startingPoint endPointNumber

let getTaxicabDistance x y =
    System.Math.Abs(fst x.Coordinates - fst y.Coordinates) + System.Math.Abs(snd x.Coordinates - snd y.Coordinates)

let startingPoint = {
    Coordinates = (0,0)
    PointNumber = 1
    PreviousPoint = None
}

//let lastPoint = createPlane startingPoint 347991
//getTaxicabDistance startingPoint lastPoint

let lastPoint = createPlane startingPoint 22