type RegisterOrInteger = | Register of char
                         | Integer of int64

type Instruction =  | Set of char * RegisterOrInteger
                    | Subtract of char * RegisterOrInteger
                    | Multiply of char * RegisterOrInteger
                    | Jump of RegisterOrInteger * RegisterOrInteger
with
    static member getInstructionName instr =
        match instr with
        | Set(x,y) -> sprintf "set "
        | Subtract(x,y) -> "sub"
        | Multiply(x,y) -> "mul"
        | Jump(x,y) -> "jnz"


let getCharOrInt value =
    let success, result = System.Int64.TryParse (value)
    match success with
    | false -> value |> char |> Register
    | true ->  result |> Integer

let charIntInstruction (ops:string array) =
    let register = char ops.[1]
    let value = getCharOrInt ops.[2]
    (register, value)

let parseInstruction (inst:string) =
    let ops = inst.Split(' ')
    match ops.[0].ToLower() with
    | "set" -> ops |> charIntInstruction |> Set
    | "sub" -> ops |> charIntInstruction |> Subtract
    | "mul" -> ops |> charIntInstruction |> Multiply
    | "jgz" -> (ops.[1] |> getCharOrInt, ops.[2] |> getCharOrInt) |> Jump
    | "jnz" -> (ops.[1] |> getCharOrInt, ops.[2] |> getCharOrInt) |> Jump
    | _ -> failwith "Invalid instruction"

let parseInstructions input =
    input    
        |> Array.filter (System.String.IsNullOrEmpty >> not) 
        |> Array.map (parseInstruction)

let getRegisters =
    [97..104] |> List.map (fun x -> (char x, 0L)) |> Array.ofList

let getValueFromRegName registers regName =
    registers 
        |> Array.find (fun x -> fst x = regName) 
        |> snd

let getValueFromRegOrInt registers regOrInt =
    match regOrInt with
    | Register x -> x |> getValueFromRegName registers |> int64
    | Integer x -> int64 x 

let setValueOfRegister registers regName value =
    let index = registers |> Array.findIndex (fun x -> fst x = regName)
    registers.[index] <- (regName, value)
    registers

let performBasicOperation registers regName regNameOrValue operation =
    let value = getValueFromRegOrInt registers regNameOrValue
    let newValue = operation (getValueFromRegName registers regName) (value)
    setValueOfRegister registers regName newValue

let processOperation (registers, frequencyPlaying, _, timesMultProcessed) instruction = 
    let op = Instruction.getInstructionName instruction
    printf "%s \n" op
    match instruction with
    | Subtract (x,y) -> let regs = performBasicOperation registers x y (-)
                        (regs, frequencyPlaying, int64 1, timesMultProcessed)
    | Set (x,y) -> let value = getValueFromRegOrInt registers y
                   let regs = setValueOfRegister registers x value
                   (regs, frequencyPlaying, int64 1, timesMultProcessed)
    | Multiply (x,y) -> let regs = performBasicOperation registers x y (*)
                        (regs, frequencyPlaying, int64 1, timesMultProcessed + 1)
    | Jump (x,y) -> let value = getValueFromRegOrInt registers x
                    if value <> int64 0 then
                        let offset = getValueFromRegOrInt registers y
                        (registers, frequencyPlaying, offset, timesMultProcessed)
                    else
                        (registers, frequencyPlaying, int64 1, timesMultProcessed)


let processOperationsOnRegisters registers instruction =
    let rec loop (registers, frequencyPlaying, offset, timesMultProcessed) pos =
        let newPos = int (int64 pos + offset);
        if newPos < 0 || newPos >= (instruction |> Array.length) then
            (registers, frequencyPlaying, offset, timesMultProcessed)
        else
            let operation = instruction.[newPos]
            let result = processOperation 
                            (registers, frequencyPlaying, offset, timesMultProcessed)
                            operation
            loop result newPos
    
    let (registers, frequencyPlaying, _, timesMultProcessed) = loop (registers, 0L, 0L, 0) 0
    (registers, timesMultProcessed)


let part1 input = 
    let instructions = parseInstructions input
    let registers = getRegisters
    processOperationsOnRegisters registers instructions

let part2 input =
    let instructions = parseInstructions input
    let registers = getRegisters |> Array.map (fun (x,y) -> if x = 'a' then (x, 1L) else (x,y))
    processOperationsOnRegisters registers instructions

let input = System.IO.File.ReadAllLines (System.IO.Directory.GetCurrentDirectory() + @"\AdventOfCode2017\Day23Input.txt")
input |> part1

