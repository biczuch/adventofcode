open System.Runtime.InteropServices
type Tower = {
    name : string
    weight : int
    subtowersNames: string array option
    subtowers: Tower array option
}

type GroupedTowerWeights = GroupedTowerWeights of int * int array

type BalanceOfTower = | Balanced
                      | NotBalanced of (Tower * int)[]

let parseName (inputLine:string) =
    inputLine.Substring(0, inputLine.IndexOf(" "))

let parseWeight (inputLine:string) =
    let length = inputLine.IndexOf(")") - inputLine.IndexOf("(") - 1
    inputLine.Substring(inputLine.IndexOf("(") + 1, length) |> System.Int32.Parse
    
let parseTowersNames (inputLine:string) =
    let indexOfArrow = inputLine.IndexOf("->")
    
    match indexOfArrow with
        | -1 -> None
        | _ -> let towerNamesString = inputLine.Substring(indexOfArrow + 3)
               Some(towerNamesString.Replace(", ", ",").Split(','))

let parseInputLineToTower line = 
    {
        name = parseName line
        weight = parseWeight line
        subtowersNames = parseTowersNames line
        subtowers = None
    }

let parseInput (input:string) =
    input.Split('\n') 
        |> Array.filter (System.String.IsNullOrWhiteSpace >> not)
        |> Array.map parseInputLineToTower

let getBottomTower towers =
    let subtowersOnly = towers
                        |> Array.collect (fun x -> match x.subtowersNames with
                                                       | None -> [||]
                                                       | Some(y) -> y)
    towers 
        |> Array.filter (fun x -> subtowersOnly |> Array.contains x.name |> not) 
        |> Array.exactlyOne

let rec produceTree towers towerName  =
    let tower = towers |> Array.find (fun x -> x.name = towerName)
    match tower.subtowersNames with
        | None -> tower
        | Some(x) -> let subtowers = x |> Array.map ( produceTree towers )
                     { tower with subtowers = Some(subtowers) }
    
let produceTreeFromBottom towers =
    towers |> getBottomTower |> (fun x -> x.name) |> produceTree towers

let rec calculateWeight tower =
    let folder acc x =
        acc + calculateWeight x
    match tower.subtowers with
        | None -> tower.weight
        | Some(x) -> x |> Array.fold folder tower.weight

let getBalanceOfTower tower =
    match tower.subtowers with 
        | None -> Balanced
        | Some(subtower) -> subtower 
                                |> Array.map (fun x -> (x, calculateWeight x))
                                |> NotBalanced


let groupTowers towers = 
    towers 
        |> Array.map (fun x -> (x, calculateWeight x))
        |> Array.groupBy snd

let selectUnbalancedSubtower groupedSubtowers =
    groupedSubtowers 
        |> Array.minBy (snd >> Array.length)
        |> snd
        |> Array.exactlyOne


let getUnbalancedTower towers =
    let groupedSubtowers = towers |> groupTowers
    match groupedSubtowers |> Array.length with
        | 0 -> None
        | 1 -> None
        | _ -> Some(selectUnbalancedSubtower groupedSubtowers)

let rec selectOnlyOneBadTower parent tower  =
    match tower.subtowers with
        | None -> match parent with
                    | None -> None
                    | Some(p) -> Some(p)
        | Some(x) -> match getUnbalancedTower x with
                        | None -> match parent with
                                    | None -> None
                                    | Some(p) -> Some(p)
                        | Some(x) -> fst x |> selectOnlyOneBadTower (Some(tower))
    

let towers = parseInput "pbga (66)
xhth (57)
ebii (61)
havc (66)
ktlj (57)
fwft (72) -> ktlj, cntj, xhth
qoyq (66)
padx (45) -> pbga, havc, qoyq
tknk (41) -> ugml, padx, fwft
jptl (61)
ugml (68) -> gyxo, ebii, jptl
gyxo (61)
cntj (57)"       

let removeSubtowers tower =
    {tower with subtowers = None}

let getOnlyNearTowers tower =
    match tower.subtowers with
        | None -> tower
        | Some(subtowers) -> let modifiedSubtowers = subtowers 
                                                        |> Array.map (fun x -> { x with weight = calculateWeight x })
                                                        |> Array.map removeSubtowers

                             { tower with subtowers = modifiedSubtowers |> Some }

let getBalanceForChildren tower =
    match tower.subtowers with
        | None -> None
        | Some(x) -> Some(groupTowers x |> Array.map (fun y -> snd y |> Array.map (fun z -> ((fst z).name, snd z))))

let getBalanceForChildren2 tower =
    match tower.subtowers with
        | None -> None
        | Some(x) -> x |> Array.map calculateWeight |> Some

let readable = towers 
                    |> produceTreeFromBottom 
                    |> selectOnlyOneBadTower None
                    |> fun x -> match x with
                                     | None -> None
                                     | Some(x) -> x |> getOnlyNearTowers |> Some

let getDifference towers =
    match towers with
        | None -> 0
        | Some(x) -> let grouped = x |> groupTowers 
                     let sorted = grouped |> Array.sortBy (fun x -> fst x)
                     fst sorted.[0] - fst sorted.[1]

let findNameOfBadTower towers =
        match towers with
        | None -> System.String.Empty
        | Some(x) -> let grouped = x |> groupTowers
                     let min = grouped |> Array.minBy (fun x -> snd x |> Array.length)
                     min |> snd |> Array.exactlyOne |> fst |> fun x -> x.name

let findTowerByName name towers =
    towers |> Array.find (fun x -> x.name = name )

let changeTowerValue diffValue tower =
    { tower with weight = tower.weight + diffValue}

let findAnswer towers readable = 
    match readable with
        | None -> None
        | Some(x) -> let diff = x.subtowers |> getDifference
                     
                     let nameOfBadTower = x.subtowers |> findNameOfBadTower 
                     Some(nameOfBadTower)
                     towers |> findTowerByName nameOfBadTower |> changeTowerValue diff |> Some

findAnswer towers readable




