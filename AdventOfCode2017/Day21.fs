open System.IO

let getFlippedPatterns pattern =
    let flippedVertically = pattern |> List.rev
    let flippedHorizontally = pattern |> List.map (List.rev)
    let flippedHorAndVertic = pattern |> List.rev |> List.map (List.rev)
    [flippedVertically] @ [flippedHorizontally] @ [flippedHorAndVertic]

let getRotatedPatterns pattern =
    let rec rotatePattern pattern output =
        let filteredPattern = pattern |> List.filter (List.isEmpty >> not)
        match filteredPattern with
        | [] -> output
        | _ -> let heads = pattern |> List.map (fun x -> x |> List.head) |> List.rev
               let tails = pattern |> List.map (fun x -> x |> List.tail)
               rotatePattern tails (output @ [heads])
    let x1 = rotatePattern pattern []
    let x2 = rotatePattern x1 []
    let x3 = rotatePattern x2 []
    [x1] @ [x2] @ [x3]

let getAllPatterns pattern = 
    let rotatedPatterns = pattern |> getRotatedPatterns
    let flippedRotatedPatterns = rotatedPatterns |> List.collect getFlippedPatterns
    [pattern] @ rotatedPatterns @ flippedRotatedPatterns |> List.distinct

type Image<'a> = Image of 'a list list

type Rule<'a> = {
    inputPatterns : Image<'a> list
    outputPattern : Image<'a> 
}
with static member CreateRule initialPattern outputPattern =
    { 
        inputPatterns = getAllPatterns initialPattern |> List.map Image; 
        outputPattern = outputPattern
    }

let gridSizeIsDivisibleby size grid =
    let isProperHeight = (grid |> List.length) % size = 0
    let isProperWidth = grid |> List.forall ((fun x -> (x |> List.length) % size = 0))
    isProperHeight && isProperWidth

let breakInto2x2Grids grid =
    if (grid |> (gridSizeIsDivisibleby 2 >> not)) then
        failwith "Invalid grid size"

    let rec get2x2Grids grid output row =
        match grid with
        | [] -> output
        | x::y::gs -> let x1::x2::xs = x
                      let y1::y2::ys = y
                      let grid2x2 = [[x1;x2];[y1;y2]];
                      let lesserGrid = [xs] @ [ys] @ gs |> List.filter (List.isEmpty >> not)
                      let nextRow = row @ [grid2x2]
                      if xs |> List.isEmpty then  
                          get2x2Grids lesserGrid (output @ [nextRow]) []   
                      else
                          get2x2Grids lesserGrid output nextRow
    get2x2Grids grid [] []

let breakInto3x3Grids grid =
    if (grid |> (gridSizeIsDivisibleby 3 >> not)) then
        failwith "Invalid grid size"

    let rec get3x3Grids grid output row =
        match grid with
        | [] -> output
        | x::y::z::gs -> let x1::x2::x3::xs = x
                         let y1::y2::y3::ys = y
                         let z1::z2::z3::zs = z
                         let grid3x3 = [[x1;x2;x3];[y1;y2;y3];[z1;z2;z3]];
                         
                         let lesserGrid = [xs] @ [ys] @ [zs] @ gs |> List.filter (List.isEmpty >> not)
                         let nextRow = row @ [grid3x3]
                         if xs |> List.isEmpty then  
                             get3x3Grids lesserGrid (output @ [nextRow]) []   
                         else
                             get3x3Grids lesserGrid output nextRow
                             
    get3x3Grids grid [] []

let parsePattern (patternStr:string) =
    patternStr.Split('/') 
        |> Array.map(fun x -> x.ToCharArray() |> List.ofArray) 
        |> List.ofArray

let parseRule (ruleStr:string) = 
    let rulePattern::outputPattern::[] = ruleStr.Replace("\r", "").Replace(" ", "").Replace("=>", ";").Split(';') 
                                         |> Array.map parsePattern
                                         |> List.ofArray
    Rule.CreateRule rulePattern (outputPattern |> Image)

let breakImage image =
    let (Image imageAsGrid) = image
    if gridSizeIsDivisibleby 2 imageAsGrid then
        breakInto2x2Grids imageAsGrid
    else if gridSizeIsDivisibleby 3 imageAsGrid then
        breakInto3x3Grids imageAsGrid
    else failwith "Invalid grid size!"


let transformImage rules image =
    match rules |> List.tryFind (fun x -> x.inputPatterns |> List.contains image) with
    | None -> failwith "????????????"
    | Some(rule) -> rule.outputPattern

let mergeImage (Image image1) (Image image2) =
    List.zip image1 image2 |> List.map (fun (x,y) -> x @ y) |> Image


let convertToWholeImage (imageList:Image<'a> list list) =
    imageList 
        |> List.map (fun x -> x |> List.reduce (mergeImage))
        |> List.map (fun (Image img) -> img )
        |> List.collect (id)
        |> Image

let rec createOutputImage initImage iterations rules =
    printf "Iteration: %d\n" iterations
    match iterations with
    | 0 -> initImage
    | _ -> let newImage = breakImage initImage
                            |> List.map (fun x -> x |> List.map (fun x -> x |> Image |> transformImage rules))
                            |> fun x -> x |> convertToWholeImage

           createOutputImage newImage (iterations - 1) rules 


let initialImage = 
    [['.';'#';'.']
    ;['.';'.';'#']
    ;['#';'#';'#']] |> Image


let parseInput (input:string) =
    input.Split('\n')
        |> Array.map parseRule 
        |> List.ofArray

let solve input iterations =
    let (Image outputImage) = parseInput input |> createOutputImage initialImage iterations
    outputImage |> List.collect id |> List.filter (fun x -> x = '#') |> List.length


let input = File.ReadAllText (System.IO.Directory.GetCurrentDirectory() + @"\Day21Input.txt")
let part1 = solve input 5
let part2 = solve input 18
(part1, part2)