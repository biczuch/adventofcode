open System;

type Directions = 
    | North
    | NorthWest
    | NorthEast
    | South
    | SouthWest
    | SouthEast
    with 
        static member ParseDirection directionString =
            match directionString with
            | "s" -> South
            | "ne" -> NorthEast
            | "nw" -> NorthWest
            | "sw" -> SouthWest
            | "se" -> SouthEast
            | "n" -> North
            | _ -> failwith "Direction not supported"

let parseInput (input:string) =
    input.Split(',')
        |> Array.map (string >> Directions.ParseDirection)
        |> List.ofArray


let moveInDirection position direction =
    let (x,y) = position
    match direction with
    | North -> (x, y - 1)
    | NorthWest -> (x - 1, y)
    | NorthEast -> (x + 1, y - 1)
    | South -> (x, y + 1)
    | SouthWest -> (x - 1, y + 1)
    | SouthEast -> (x + 1, y)

let rec findPositionFromSteps startPoint steps =
    match steps with
    | [] -> startPoint
    | x::xs -> findPositionFromSteps
                   (moveInDirection startPoint x)
                   xs

let cubeDistance (a:(int*int*int)) (b:(int*int*int)) =
    let (x,y,z) = a
    let (x1,y1,z1) = b
    [ Math.Abs(x - x1); Math.Abs(y - y1); Math.Abs(z - z1)]
        |> List.max

let axialToCube hex =
    let (q, r) = hex
    let x = q
    let z = r
    let y = -x-z
    (x, y, z)

let hexDistance a b =
    let ac = axialToCube a
    let bc = axialToCube b
    cubeDistance ac bc

let howFarEver startPos (directions:Directions list) = 
    let mutable howFarEver = 0
    let startPositionConst = startPos
    let rec loop startPos (directions:Directions list) =
        match directions with 
        | [] -> []
        | x::xs -> let newPosition = (moveInDirection startPos x)
                   let distance = hexDistance startPositionConst newPosition
                   match distance > howFarEver with
                           | true -> howFarEver <- distance
                           | _ -> howFarEver <- howFarEver
                   |> ignore
                   loop newPosition xs
    
    loop startPos directions |> ignore
    howFarEver

let input = IO.File.ReadAllText(IO.Directory.GetCurrentDirectory() + @"\Day12Input.txt")

let part1 = parseInput 
            >> findPositionFromSteps (0,0) 
            >> hexDistance (0,0)
let part2 = parseInput >> howFarEver (0,0)

input |> part1
input |> part2