type Particle = {
    position : int * int * int
    velocity : int * int * int
    acceleration: int * int * int
    distance: int
}
with 
    static member CreateParticle (x,y,z) (vx,vy,vz) (ax,ay,az) =
        { position = (x,y,z); velocity = (vx,vy,vz); acceleration = (ax,ay,az); distance = 99999 }

let calculateManhattanDistanceFrom000 particle =
    let (x,y,z) = particle.position
    System.Math.Abs(x) + System.Math.Abs(y) + System.Math.Abs(z)

let updateDistance particle =
    let distance = calculateManhattanDistanceFrom000 particle
    { particle with distance = distance }

let updateVelocity particle =
    let (vx,vy,vz) = particle.velocity
    let (ax,ay,az) = particle.acceleration
    { particle with velocity = (vx + ax, vy + ay, vz + az) }

let updatePosition particle =
    let (vx,vy,vz) = particle.velocity
    let (x,y,z) = particle.position
    { particle with position = (x + vx, y + vy, z + vz) }

let updateParticle = updateVelocity >> updatePosition >> updateDistance

let parseValues (posString:string) =
    let [|x;y;z|] = posString.Remove(0,1).Replace("=<", "").Replace(">", "").Split(',') |> Array.map int
    (x,y,z)

let parseLine (line:string) = 
    match (line.Replace(" ", "").Replace(">,", ";").Split(';')) with
    | [|pos;vel;acc|] -> let position = parseValues pos
                         let velocity = parseValues vel
                         let acceleration = parseValues acc
                         Particle.CreateParticle position velocity acceleration
    | _ -> failwith "Invalid input line"
    
let parseInput (input:string) =
    input.Split('\n') 
        |> Array.map (parseLine)
        |> List.ofArray

let updateLastTenLeastDistance particles lastTenLeastDistance = 
    let leastDistance = particles |> List.minBy (fun x -> (snd x).distance)
    match lastTenLeastDistance |> List.length with
    | x when x < 1000 -> lastTenLeastDistance @ [leastDistance]
    | x when x = 1000 -> lastTenLeastDistance.Tail @ [leastDistance]
    | _ -> failwith "Something went wrong on updating least ten distance"

let path = System.IO.Directory.GetCurrentDirectory() + @"\Day20Input.txt"
let input = System.IO.File.ReadAllText(path)

let part1 input =
    let particles = input |> parseInput |> List.mapi (fun i x -> (i, x))
    let rec loop particles lastTenLeastDistance =
        let lenght = lastTenLeastDistance |> List.distinctBy (fst) |> List.length
        match lenght with
        | 1 when lastTenLeastDistance.Length = 1000 -> lastTenLeastDistance.Head
        | _ -> let newParticles = particles |> List.map (fun x -> (fst x, updateParticle (snd x)))
               let lastTen = updateLastTenLeastDistance newParticles lastTenLeastDistance
               loop newParticles lastTen
        
    let updatedParticles = loop particles []
    updatedParticles 
        
let part2 input = 
    let particles = input |> parseInput |> List.mapi (fun i x -> (i, x))
    let rec loop particles lastTenLeastDistance =
        let lenght = lastTenLeastDistance |> List.distinctBy (fst) |> List.length
        match lenght with
        | 1 when lastTenLeastDistance.Length = 1000 -> particles |> List.length
        | _ -> let newParticles = particles 
                                    |> List.map (fun x -> (fst x, updateParticle (snd x)))
                                    |> List.distinctBy (fun x -> snd x |> (fun p -> p.position))
               let lastTen = updateLastTenLeastDistance newParticles lastTenLeastDistance
               loop newParticles lastTen
        
    let updatedParticles = loop particles []
    updatedParticles 

input |> part2

