open System.IO
 
type Programs = Programs of Map<int, int list>
 
let parseRow (input:string) =
    let (x::xs) = input.Replace("<->", ",").Replace(" ", "").Split(',')
                |> Array.map int
                |> List.ofArray
    (x, xs)
      
 
let parseInput (input:string) =
    input.Split('\n')
        |> Array.filter (System.String.IsNullOrWhiteSpace >> not)
        |> Array.map (parseRow)
        |> Map.ofArray
        |> Programs

let setToListFolder (acc:Set<'T>) x =
    acc.Add x

let getVisitedPrograms (startProgram:int) (programs:Programs) =
    let (Programs programsMap) = programs
 
    let rec loop (startProgramNo:int) (programs:Map<int, list<int>>) (visitedPrograms:Set<int>) =
        let visited = visitedPrograms.Add(startProgramNo)
        match programs.TryFind startProgramNo with
        | None -> visited
        | Some(linkedPrograms) -> linkedPrograms
                                    |> List.filter (visited.Contains >> not)
                                    |> List.fold (fun acc x -> loop x programs visited 
                                                                |> Set.fold(setToListFolder) acc) visited

    loop startProgram programsMap Set.empty
 
let count0Programs = getVisitedPrograms 0 >> Set.count
 
let getGroups programs =
    let rec loop progs =
        match progs with
        | [] -> []
        | (x::xs) -> [getVisitedPrograms x programs] @ loop xs
   
    let (Programs programsMap) = programs  
    let progs = programsMap |> Map.toList |> List.map (fst)
    loop progs |> List.distinct
 
let countGroups = getGroups >> List.length
 

let input = File.ReadAllText(Directory.GetCurrentDirectory() + @"\Day12Input.txt")
let part1 = input |> parseInput |> count0Programs
let part2 = input |> parseInput |> countGroups