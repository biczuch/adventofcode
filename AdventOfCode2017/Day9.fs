open System.IO
let rec removeExclemationMarks = function
    | [] -> []
    | [x] -> [x]
    | x::y::xs -> if x = '!' then
                    removeExclemationMarks xs
                  else
                    [x] @ removeExclemationMarks ([y] @ xs)

let stringToCharArray (string:string) =
    string.ToCharArray()

let concatenateListElements = List.fold (fun acc x -> acc + x.ToString()) ""

let rec removeDuplicatesOf duplicateChar input =
    match input with
        | [] -> []
        | [x] -> [x]
        | x::y::xs -> if x = duplicateChar && x = y then
                          removeDuplicatesOf duplicateChar ([x] @ xs)
                      else
                          [x] @ removeDuplicatesOf duplicateChar ([y] @ xs)

let removeIgnoredCharacters =
     stringToCharArray >> List.ofArray >> removeExclemationMarks >> concatenateListElements

let ignoreExtraLesserThancharacters =
    stringToCharArray >> List.ofArray >> (removeDuplicatesOf '<') >> concatenateListElements

let rec removeGarbageContent' = function
    | [] -> []
    | [x] -> [x]
    | x::xs -> if x = '<' then
                    let indexOfClosing = xs |> List.findIndex ((=) '>')
                    let newTail = xs |> List.skip (indexOfClosing)
                    let h::t = newTail
                    [x] @ [h] @ removeGarbageContent' t
                 else
                    [x] @ removeGarbageContent' xs

let removeGarbageContent =
    stringToCharArray >> List.ofArray >> removeGarbageContent' >> concatenateListElements

let reduceInput = removeIgnoredCharacters >> ignoreExtraLesserThancharacters >> removeGarbageContent

let rec calculateScore' localScore input  =
    match input with
        | [] -> 0
        | x::xs -> match x with
                       | '{' -> localScore + calculateScore' (localScore + 1) xs 
                       | '}' -> calculateScore' (localScore - 1) xs 
                       | _ -> calculateScore' localScore xs 
let calculateScore =
    stringToCharArray >> List.ofArray >>  calculateScore' 1

let rec countGarbageContent' = function
    | [] -> 0
    | [_] -> 0
    | x::xs ->  if x = '<' then
                    let garbageCount = xs |> List.findIndex ((=) '>')
                    let newTail = xs |> List.skip (garbageCount)
                    let h::t = newTail
                    garbageCount + countGarbageContent' t
                else if x = '}' || x = '{' || x = ',' then
                    countGarbageContent' xs
                else
                    1 + countGarbageContent' xs

let countGarbage = removeIgnoredCharacters >> stringToCharArray >> List.ofArray >> countGarbageContent'

let path = Path.Combine(Directory.GetCurrentDirectory(), "\\Day9Input.txt");
let riddleInput = File.ReadAllText path
let part1 = riddleInput |> reduceInput |> calculateScore
let part2 = riddleInput |> countGarbage



