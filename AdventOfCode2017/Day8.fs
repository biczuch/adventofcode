type HighestValue = HighestValue of int
        with
        static member Deconstruct x =
            x |> fun (HighestValue x) -> x

type Value = Value of int
    with
        static member Deconstruct x =
            x |> fun (Value x) -> x

type Registers = Registers of Map<string, (Value * HighestValue)>

type Operation = Operation of (int -> int -> int)
    with
        static member Deconstruct x =
            x |> fun (Operation x) -> x
type Comparator = Comparator of (int -> int -> bool)
    with
        static member Deconstruct x =
            x |> fun (Comparator x) -> x
 
type Instruction = {
    operatorA : string
    operatorB : int
    operation : Operation
    comparatorA : string
    comparator : Comparator
    ComparatorB : int
}
 
let parseComparator = function
    | "==" -> (=)
    | "!=" -> (<>)
    | "<" -> (<)
    | "<=" -> (<=)
    | ">" -> (>)
    | ">=" -> (>=)
    | _ -> failwith "Invalid comparator"
 
let parseOperation = function
    | "inc" -> (+)
    | "dec" -> (-)
    | _ -> failwith "Invalid operator"

let parseRow (row:string) =
    let splitted = row.Split(' ');
    {
        operatorA = splitted.[0]
        operatorB = splitted.[2] |> int
        operation = splitted.[1] |> parseOperation |> Operation
        comparatorA = splitted.[4]
        comparator = splitted.[5] |> parseComparator |> Comparator
        ComparatorB = splitted.[6] |> int
    }
 
let parseInput (input:string) =
    input.Split('\n')
        |> Array.filter (System.String.IsNullOrWhiteSpace >> not)
        |> Array.map parseRow
        |> List.ofArray
 
 
let addInstructionVariablesIfNecessary (registers:Registers) (instruction:Instruction) =
    let (Registers regs) = registers
    let variableNames = [instruction.operatorA; instruction.comparatorA]
 
    let folder (acc:Map <string, Value*HighestValue>) x =
        match acc.TryFind x with
            | None -> acc.Add (x, (0 |> Value, 0 |> HighestValue))
            | Some(_) -> acc
 
    variableNames |> List.fold folder regs |> Registers
 
let getValue (registers : Registers) key =
    let (Registers regs) = registers;
    regs |> Map.find key |> (fst >> Value.Deconstruct)
   
 
let modifyRegisters registers registerName newValue =
    let (Registers regs) = registers
    let oldHighestValue = regs |> Map.find registerName |> snd |> HighestValue.Deconstruct
    let highestValue = if newValue > oldHighestValue then newValue else oldHighestValue
    regs 
        |> Map.remove registerName 
        |> Map.add registerName (newValue |> Value , highestValue |> HighestValue)
        |> Registers


let modifyRegister instruction registers  =
    let comparerAValue = instruction.comparatorA |> getValue registers

    if comparerAValue |> (instruction.comparator |> Comparator.Deconstruct) <| instruction.ComparatorB then
        let value = getValue registers instruction.operatorA
        let (Operation operation) = instruction.operation
        let newValue = value |> operation <| instruction.operatorB
        modifyRegisters registers instruction.operatorA newValue
    else
        registers
 
let processInstruction registers instruction =
    instruction 
        |> addInstructionVariablesIfNecessary registers
        |> modifyRegister instruction

let processAllInstructions instructions =
    let initialRegisters = Map.empty |> Registers
    instructions |> List.fold processInstruction initialRegisters

let getValuesFromRegisters registers =
    let (Registers regs) = registers
    regs |> Map.fold (fun acc k v -> acc @ [v]) []
    
let findLargestValueInRegister = getValuesFromRegisters >> List.maxBy (fst >> Value.Deconstruct) >> fst
let findLargestValueEverHeld = getValuesFromRegisters >> List.maxBy (snd >> HighestValue.Deconstruct) >> snd

let input = "b inc 5 if a > 1
a inc 1 if b < 5
c dec -10 if a >= 1
c inc -20 if c == 10"

let part1 = parseInput >> processAllInstructions >> findLargestValueInRegister
input |> part1
let part2 = parseInput >> processAllInstructions >> findLargestValueEverHeld
input |> part2