type Generator = {
    currentValue : uint64
    factor: uint64
    multiplies: uint64
}
with
    static member CreateGenerator startNumber factor multiplies =
        {
            currentValue = startNumber
            factor = factor
            multiplies = multiplies
        }

let generateNextGeneratorValue (generator:Generator) =
    let x = ((generator.currentValue * generator.factor) % (uint64 2147483647))
    { generator with currentValue = x }

let rec generateNextGeneratorValueUntilDivisibleBy4 (generator:Generator) =
    let x = ((generator.currentValue * generator.factor) % (uint64 2147483647))
    let nextGenerator ={ generator with currentValue = x }
    match nextGenerator.currentValue % nextGenerator.multiplies = (uint64 0) with
    | true -> nextGenerator
    | false -> generateNextGeneratorValueUntilDivisibleBy4 nextGenerator

let compareDigits (x:uint64) (y:uint64) =
    let fourteenBinaryDigits = (uint64) 0xFFFF
    (x &&& fourteenBinaryDigits) = (y &&& fourteenBinaryDigits)

let getNextGeneratorsValues (generatorA, generatorB) =
    let a = generateNextGeneratorValue generatorA
    let b = generateNextGeneratorValue generatorB
    (a,b)

let rec getNextGeneratorsValues2 (generatorA, generatorB) =
    let a = generateNextGeneratorValueUntilDivisibleBy4 generatorA
    let b = generateNextGeneratorValueUntilDivisibleBy4 generatorB
    (a,b)

let rec getCount valuesGenerator (generatorA,generatorB) iterations count=
    match iterations with
    | 0 -> count
    | _ -> let (a,b) = valuesGenerator (generatorA, generatorB)
           let newCount = match compareDigits a.currentValue b.currentValue with
                          | true -> count + 1; 
                          | false -> count
               
           getCount valuesGenerator (a,b) (iterations - 1) newCount


let generatorA = Generator.CreateGenerator (uint64 618) (uint64 16807) (uint64 4)
let generatorB = Generator.CreateGenerator (uint64 814) (uint64 48271) (uint64 8)
getCount getNextGeneratorsValues2 (generatorA, generatorB) 5000000 0


