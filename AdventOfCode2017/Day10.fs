type State = PrepareLengths | ReverseOrder | MoveCurrentPosition | IncreaseSkipSize | Stop

type StateMachine = {
    elements : int list
    currentPosition : int
    lengths : int list
    initialLenghts: int list
    currentLength : int
    skipSize : int
    state : State
    step : int
}
with
    static member CreateStateMachine lengths =
        {
            elements = [0..255]
            currentPosition = 0
            lengths = lengths
            initialLenghts = lengths
            currentLength = 0
            skipSize = 0
            state = PrepareLengths
            step = 1
        }

let parseInput (input:string) =
    input.Replace(" ", "").Split(',') |> Array.map int |> List.ofArray

let parseInputPart2 (input:string) =
    let suffix = [17; 31; 73; 47; 23]
    input 
        |> Seq.map int 
        |> List.ofSeq 
        |> fun x -> (x @ suffix)

let getElementsFromList list startPos length =
    (list @ list) |> List.skip startPos |> List.take (length % list.Length)

let replaceElementAtIndex index newElement list =
    list |> List.mapi (fun i x -> if i = index then newElement else x)

let rec replaceList startPos list listToReplace =
    match list with
        | [] -> listToReplace 
        | x::xs -> listToReplace 
                       |> replaceElementAtIndex startPos x
                       |> replaceList ((startPos + 1) % (listToReplace |> List.length)) xs

let pickAndReverseOrder stateMachine = 
    getElementsFromList stateMachine.elements stateMachine.currentPosition stateMachine.currentLength
        |> List.rev


let replaceStateMachineList stateMachine withList =
    let newElements = replaceList stateMachine.currentPosition withList stateMachine.elements
    { stateMachine with elements = newElements }

let processReverseOrderState stateMachine = 
    pickAndReverseOrder stateMachine |> replaceStateMachineList stateMachine

let increaseSkipSize stateMachine = 
    { stateMachine with skipSize = stateMachine.skipSize + 1 }

let moveCurrentPosition stateMachine =
    let newPosition = (stateMachine.currentPosition + stateMachine.currentLength + stateMachine.skipSize) % stateMachine.elements.Length
    { stateMachine with currentPosition = newPosition }

let moveToNextState stateMachine =
    match stateMachine.state with
        | PrepareLengths -> {stateMachine with state = ReverseOrder}
        | ReverseOrder -> { stateMachine with state = MoveCurrentPosition}
        | MoveCurrentPosition -> { stateMachine with state = IncreaseSkipSize}
        | IncreaseSkipSize -> stateMachine 
                                |> (fun x -> match x.lengths with
                                                | [] -> { stateMachine with state = Stop }
                                                | _ ->  { stateMachine with state = PrepareLengths })
                                
        | Stop -> stateMachine

let removeCurrentLenghtFromLenghts stateMachine =
    { stateMachine with lengths = stateMachine.lengths |> List.tail }

let rec ProcessStateMachine stateMachine =
    let processedState = match stateMachine.state with
                            | PrepareLengths -> { stateMachine with currentLength = stateMachine.lengths |> List.head } 
                            | ReverseOrder -> stateMachine |> processReverseOrderState  
                            | MoveCurrentPosition -> stateMachine |> moveCurrentPosition
                            | IncreaseSkipSize ->   stateMachine 
                                                        |> increaseSkipSize
                                                        |> removeCurrentLenghtFromLenghts
                                                        |> fun x -> { x with step = x.step + 1 }

                            | Stop -> stateMachine

    let newState = processedState |> moveToNextState

    match newState.state with
        | Stop -> newState
        | _ -> newState |> ProcessStateMachine
    
let multiplyFirst2Elements stateMachine =
    match stateMachine.elements with
        | x::y::_ -> Some(x*y)
        | _ -> None

let reapplyLenghts stateMachine = 
    { stateMachine with lengths = stateMachine.initialLenghts }

let reapplyState stateMachine =
    { stateMachine with state = PrepareLengths }

let rec runRounds rounds stateMachine =
    match rounds with
        | x when x < 0 -> failwith "Invalid rounds number"
        | x when x = 0 -> stateMachine
        | _ -> stateMachine
                |> ProcessStateMachine
                |> reapplyLenghts
                |> reapplyState
                |> runRounds (rounds - 1)

let sparseHash = runRounds 64 >> (fun x -> x.elements)

let xorValues list = list |> List.reduce (^^^)

let denseHash = List.splitInto 16 >> List.map xorValues

let convertToHash = 
    List.map (byte >> fun x -> System.String.Format("{0:X2}",x))
        >> List.reduce (+)


let input = "130,126,1,11,140,2,255,207,18,254,246,164,29,104,0,224" |> parseInput
let input2 = "130,126,1,11,140,2,255,207,18,254,246,164,29,104,0,224"|> parseInputPart2

let part1 = StateMachine.CreateStateMachine input |> ProcessStateMachine |> multiplyFirst2Elements
let part2 = StateMachine.CreateStateMachine input2 
                |> sparseHash 
                |> denseHash 
                |> convertToHash

